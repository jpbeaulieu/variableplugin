//
//  VariablePlugin.h
//  WorldAffair
//
//  Created by Jean-Philippe Beaulieu on 2014-11-10.
//
//

#import <Cordova/CDV.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationService.h"
#import "User.h"
#import "ClientApi.h"

@interface VariablePlugin : CDVPlugin

- (void)setId:(CDVInvokedUrlCommand*)command;
- (void)setSecret:(CDVInvokedUrlCommand*)command;
- (void)getGeoloc:(CDVInvokedUrlCommand*)command;

@end
