//
//  VariablePlugin.m
//  WorldAffair
//
//  Created by Jean-Philippe Beaulieu on 2014-11-10.
//
//

#import "VariablePlugin.h"

@implementation VariablePlugin

- (void)setId:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    
        NSString* arg = [command.arguments objectAtIndex:0];
        [User setUserId:arg.intValue];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)setSecret:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    
        NSString* arg = [command.arguments objectAtIndex:0];
        [User setSecret:arg];
    
        int userId = [User getUserId];
        if(userId != 0) {
            [ClientApi setPlatformWithName:@"ios"];
            [[LocationService sharedInstance] startGeoloc];
        }
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getGeoloc:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
    
        double lat = [[LocationService sharedInstance] getLatitude];
        double lon = [[LocationService sharedInstance] getLongitude];
        
        NSLog([NSString stringWithFormat:@"%.8f", lat]);
        NSLog([NSString stringWithFormat:@"%.8f", lon]);
        
        NSNumber *latitude = [NSNumber numberWithFloat: lat];
        NSNumber *longitude = [NSNumber numberWithFloat: lon];
        
        NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:latitude,@"latitude",longitude,@"longitude", nil];
            
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:json];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
     }];
}

@end
