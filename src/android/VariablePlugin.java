package com.worldaffair.variableplugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.worldaffair.service.Singleton;

import android.util.Log;

public class VariablePlugin extends CordovaPlugin {
	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("setId")) {
            int value = args.getInt(0);
            Singleton.getInstance().setUserId(value);
            Log.e("UserId", String.valueOf(value));
            callbackContext.success();
            return true;
        }
        else if(action.equals("setSecret")) {
        	String value = args.getString(0);
        	Singleton.getInstance().setSecret(value);
        	Log.e("Secret", value);
        	callbackContext.success();
        	return true;
        }
        else if(action.equals("getGeoloc")) {
        	double latitude = Singleton.getInstance().mCallLatitude;
        	double longitude = Singleton.getInstance().mCallLongitude;
        	
        	JSONObject json = new JSONObject();
        	json.put("latitude", latitude);
        	json.put("longitude", longitude);

        	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, json));
        	
        	return true;
        }
        return false;
    }

}
