var VariablePlugin = { 
    setId: function (success, fail, resultType) {
    	if(typeof cordova !== 'undefined')
      		return cordova.exec(success, fail, "VariablePlugin", "setId", [resultType]); 
    },
    setSecret: function (success, fail, resultType) { 
    	if(typeof cordova !== 'undefined')
      		return cordova.exec(success, fail, "VariablePlugin", "setSecret", [resultType]); 
    },
    getGeoloc: function (success, fail, resultType) { 
    	if(typeof cordova !== 'undefined')
      		return cordova.exec(success, fail, "VariablePlugin", "getGeoloc", [resultType]);
      	else {
      		var json = {
      			latitude : "45.70340",
      			longitude : "-73.68567"
      		}
      		success.call(this, json);
      	} 
    }
}
module.exports = VariablePlugin;